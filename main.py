import datetime
import json

import geojson
import overpy
import shapely
from jinja2 import Environment, FileSystemLoader
from pyproj import CRS, Transformer
from shapely import geometry
from shapely.geometry import shape
from shapely.geometry.polygon import Polygon
from shapely.ops import transform

project_to_meter = Transformer.from_crs(
    CRS.from_epsg(4326),
    CRS.from_epsg(3857),
    always_xy=True)
project_from_meter = Transformer.from_crs(
    CRS.from_epsg(3857),
    CRS.from_epsg(4326),
    always_xy=True)

cities = {
    'Beograd': {
        'name': 'Beograd',
        'short': 'bg',
        'search_bbox': '44.640480,20.141946,44.974065,20.805179',
        'city_center': '[20.43, 44.81]',
        'map_bounds': '[20.141946, 44.640480], [20.805179, 44.974065]',
        'map_zoom': 11,
    },
    'Novi Sad': {
        'name': 'Novi Sad',
        'short': 'ns',
        'search_bbox': '45.149733,19.5563618,45.4314629,20.1044599',
        'city_center': '[19.85, 45.25]',
        'map_bounds': '[19.5, 45.2], [20.2, 45.4]',
        'map_zoom': 12,
    },
    'Nis': {
        'name': 'Niš',
        'short': 'ni',
        'search_bbox': '43.208945,21.7087987,43.4825933,22.1756915',
        'city_center': '[21.9, 43.32]',
        'map_bounds': '[21.7, 43.2], [22.12, 43.42]',
        'map_zoom': 12,
    },
    'Zrenjanin': {
        'name': 'Zrenjanin',
        'short': 'zr',
        'search_bbox': '45.3,20.25,45.43,20.51', # 20.254601,45.302041,20.505888,45.429837
        'city_center': '[20.4, 45.38]',
        'map_bounds': '[20.25, 45.3], [20.6, 45.45]',
        'map_zoom': 14,
    }
}


def get_bike_path_from_overpass(overpass_api, bbox):
    entities = []
    response = overpass_api.query("""
        [bbox:{0}]
        [out:json];
        (
          way["highway"="cycleway"];
          // isto kao i ono iznad, samo treba jos isfiltrirati
          way["highway"!="cycleway"]["highway"!="proposed"]["route"!="ferry"]["bicycle"="designated"]["floating"!="yes"];
          // sve ispod je za odvojene staze za bicikliste na ulici
          way["highway"!="cycleway"]["cycleway"="lane"];
          way["highway"!="cycleway"]["cycleway:both"="lane"];
          way["highway"!="cycleway"]["cycleway:left"="lane"];
          way["highway"!="cycleway"]["cycleway:right"="lane"];
        );
        (._;>;);
        out body;
    """.format(bbox))

    for w in response.ways:
        ls_coords = []
        for node in w.nodes:
            ls_coords.append((node.lon, node.lat))
        if len(w.nodes) == 1:
            geom = geometry.Point((w.nodes[0].lon, w.nodes[0].lat))
        elif w.nodes[0].id != w.nodes[-1].id:
            geom = geometry.LineString(ls_coords)
        else:
            geom = geometry.Polygon(ls_coords)
        if geom.geom_type != 'LineString':
            continue
        entities.append({
            'osm_id': 'w' + str(w.id),
            'tags': w.tags.copy(),
            'geometry': geom
        })
    return entities


def generate_offset_lanes(bike_paths):
    new_bike_paths = []
    # Offset lanes
    for bike_path in bike_paths:
        is_double_lane = \
            ('cycleway' in bike_path['tags'] and bike_path['tags']['cycleway'] == 'lane') or \
            ('cycleway:both' in bike_path['tags'] and bike_path['tags']['cycleway:both'] == 'lane')
        is_right_lane = ('cycleway:right' in bike_path['tags'] and bike_path['tags']['cycleway:right'] == 'lane') or \
                        is_double_lane
        is_left_lane = ('cycleway:left' in bike_path['tags'] and bike_path['tags']['cycleway:left'] == 'lane') or \
                       is_double_lane
        if not is_left_lane and not is_right_lane:
            new_bike_paths.append(bike_path)
        if is_left_lane:
            bike_path_meter = transform(project_to_meter.transform, bike_path['geometry'])
            bike_path_meter_offset = shapely.offset_curve(bike_path_meter, distance=3)
            bike_path_offset = transform(project_from_meter.transform, bike_path_meter_offset)
            new_bike_paths.append({
                'osm_id': bike_path['osm_id'],
                'tags': bike_path['tags'],
                'geometry': bike_path_offset
            })
        if is_right_lane:
            bike_path_meter = transform(project_to_meter.transform, bike_path['geometry'])
            bike_path_meter_offset = shapely.offset_curve(bike_path_meter, distance=-3)
            bike_path_offset = transform(project_from_meter.transform, bike_path_meter_offset)
            new_bike_paths.append({
                'osm_id': bike_path['osm_id'],
                'tags': bike_path['tags'],
                'geometry': bike_path_offset
            })
    return new_bike_paths


def is_designated(bike_path):
    is_cycleway = 'highway' in bike_path['tags'] and bike_path['tags']['highway'] == 'cycleway'
    is_segragated = 'segregated' in bike_path['tags'] and bike_path['tags']['segregated'] == 'yes'
    return is_cycleway or is_segragated


def get_surface(bike_path):
    if 'surface' not in bike_path['tags']:
        return 'unknown'
    surface_str = bike_path['tags']['surface']
    if surface_str == 'asphalt':
        return 'asphalt'
    elif surface_str == 'paved':
        return 'paved'
    elif surface_str == 'paving_stones':
        return 'paving_stones'
    elif surface_str == 'concrete' or surface_str == 'concrete:lanes':
        return 'concrete'
    elif surface_str == 'sett':
        return 'sett'
    elif surface_str == 'ground' or surface_str == 'unpaved':
        return 'ground'
    print(f'Found unrecognized surface {surface_str} in way https://openstreetmap.org/way/{bike_path["osm_id"][1:]}')
    return 'unknown'


def get_lit(bike_path):
    if 'lit' not in bike_path['tags']:
        return 'unknown'
    lit_str = bike_path['tags']['lit']
    if lit_str == 'yes':
        return 'yes'
    elif lit_str == 'no':
        return 'no'
    print(f'Found unrecognized lit {lit_str} in way https://openstreetmap.org/way/{bike_path["osm_id"][1:]}')
    return 'unknown'


def normalize_tags(bike_paths):
    new_bike_paths = []
    for bike_path in bike_paths:
        surface = get_surface(bike_path)
        lit = get_lit(bike_path)
        new_bike_paths.append({
            'osm_id': bike_path['osm_id'],
            'tags': bike_path['tags'] | {'surface': surface, 'lit': lit},
            'geometry': bike_path['geometry']
        })
    return new_bike_paths


def get_statistics(bike_paths, city):
    statistics = {
        'date_generation': datetime.datetime.now().strftime('%d.%m.%Y %Hh'),
        'total': 0,
        'paths': 0,
        'lanes': 0,
        'lit': {'yes': 0, 'no': 0, 'unknown': 0},
        'surface': {'asphalt': 0, 'paved': 0, 'paving_stones': 0, 'concrete': 0, 'sett': 0, 'ground': 0, 'unknown': 0}
    }

    with open(f'{city.lower()}.geojson', 'r') as city_json:
        city_json_dict = geojson.load(city_json)
        city_polygon: Polygon = shape(city_json_dict.features[0].geometry)
    for bike_path in bike_paths:
        city_path = city_polygon.intersection(bike_path['geometry'])
        city_path_meter = transform(project_to_meter.transform, city_path)
        length = city_path_meter.length
        statistics['total'] += length
        if is_designated(bike_path):
            statistics['paths'] += length
        else:
            statistics['lanes'] += length
        surface = bike_path['tags']['surface']
        statistics['surface'][surface] += length
        lit = bike_path['tags']['lit']
        statistics['lit'][lit] += length

    with open('statistics.json', 'w') as statistics_json:
        json.dump(statistics, statistics_json)
    return statistics


def generate_geojson(bike_paths, city_short):
    designated_features, lanes_features = [], []
    designated_collection = geojson.FeatureCollection(designated_features)
    lanes_collection = geojson.FeatureCollection(lanes_features)
    for bike_path in bike_paths:
        if is_designated(bike_path):
            designated_features.append(geojson.Feature(
                geometry=shapely.geometry.mapping(bike_path['geometry']),
                properties=bike_path['tags']
            ))
        else:
            lanes_features.append(geojson.Feature(
                geometry=shapely.geometry.mapping(bike_path['geometry']),
                properties=bike_path['tags']
            ))
    with open(f'output/{city_short.lower()}-designated.geojson', 'w') as output_json:
        geojson.dump(designated_collection, output_json)
    with open(f'output/{city_short.lower()}-lanes.geojson', 'w') as output_json:
        geojson.dump(lanes_collection, output_json)


def generate_html(statistics, city):
    env = Environment(loader=FileSystemLoader(searchpath='./templates'))
    template = env.get_template('city.html')
    output = template.render(
        statistics=statistics,
        city=city,
        cities=cities
    )
    with open(f'output/{city["short"].lower()}.html', 'w', encoding='utf-8') as fh:
        fh.write(output)


def generate_city(overpass_api, city):
    print(f'Generate for city {city["name"]}')
    bike_paths = get_bike_path_from_overpass(overpass_api, city['search_bbox'])
    bike_paths = generate_offset_lanes(bike_paths)
    bike_paths = normalize_tags(bike_paths)
    statistics = get_statistics(bike_paths, city['short'])
    generate_geojson(bike_paths, city['short'])
    generate_html(statistics, city)


def main():
    overpass_api = overpy.Overpass(url='http://localhost:12345/api/interpreter')
    for city in cities.values():
        generate_city(overpass_api, city)


if __name__ == '__main__':
    main()
