#!/bin/bash

set -euo pipefail

./venv/bin/python3 main.py

cp -R html/* output/

scp -r output/* kokanovic:/var/www/sites/projekti.openstreetmap.rs/bikepaths


