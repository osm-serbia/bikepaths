# Biciklističke staze

Ovaj projekat prikazuje biciklističke staze u gradovima u Srbiji preko vektorske mape Srbiji.

To radi tako što usrče sve podatke iz OSM-a (main.py) i onda izgeneriše HTML.

Sva pitanja, želje, čestitke i pozdrave uputite na bikesrb@kokanovic.org.